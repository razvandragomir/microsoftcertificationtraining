﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace WebApiDemo.Domain
{
    public class ValueRepository:IValueRepository
    {
        public int GetValue()
        {
            Thread.Sleep(500);
            return 3;
        }

        public async Task<int> GetValueAsync()
        {
            await Task.Delay(500);
            return 3;
        }
    }
}