﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApiDemo.Domain
{
    public interface IValueRepository
    {
        int GetValue();
        Task<int> GetValueAsync();
    }
}
