﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApiDemo.Domain;

namespace WebApiDemo.Controllers
{
    public class ValuesController : ApiController
    {
        private IValueRepository _valueRepository = new ValueRepository();

        // GET api/values
        public int DirectValue()
        {
            return 3;
        }

        [HttpGet]
        public int Get()
        {
            return _valueRepository.GetValue();
        }

        [HttpGet]
        public async Task<int> GetAsync()
        {
            return await _valueRepository.GetValueAsync();
        }

    }
}
