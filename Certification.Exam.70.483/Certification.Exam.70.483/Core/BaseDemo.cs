﻿namespace Certification.Exam.CSharp.Core
{
    public abstract class BaseDemo
    {
        public abstract void Run();
    }
}
