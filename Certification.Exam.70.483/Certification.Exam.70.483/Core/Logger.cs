﻿using System;
using System.Threading;

namespace Certification.Exam.CSharp.Core
{
    public static class Logger
    {
        public static void Debug(string format, params object[] args)
        {
            Console.WriteLine("[Thread {0}]: {1}", 
                Thread.CurrentThread.ManagedThreadId, 
                string.Format(format, args));
        }
    }
}
