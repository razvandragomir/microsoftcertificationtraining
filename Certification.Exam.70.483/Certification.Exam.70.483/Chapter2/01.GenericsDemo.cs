﻿using System.Collections.Generic;
using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter2
{
    public class GenericsDemo:BaseDemo
    {
        class CoolClass<T> where T:new()
        {
            public T CoolProperty { get; set; }

            public CoolClass()
            {
                this.CoolProperty = new T();
            }
        }

        class Person
        {
            public Person()
            {

            }
        }

        class SubPerson : Person
        {
            public SubPerson()
            {

            }
        }

        public override void Run()
        {
            List<Person> p = new List<Person>();
            p.Add(new SubPerson());

            //CoolClass<Person> myPerson = new CoolClass<SubPerson>();
            //Person coolValue = myPerson.CoolProperty;

            //myPerson = new CoolClass<Person>();
            
            // why?

            CoolClass<int> myInt = new CoolClass<int>();
        }
    }
}
