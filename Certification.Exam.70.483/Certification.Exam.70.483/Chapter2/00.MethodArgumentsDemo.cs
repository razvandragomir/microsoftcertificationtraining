﻿using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter2
{
    public class MethodArgumentsDemo: BaseDemo
    {
        void MyMethod(int a, int b=9 , int c=10, int d = 10)
        {
            Logger.Debug("{0} {1} {2} {3}", a,b,c,d);
        }

        public override void Run()
        {
            MyMethod(1, 2, 3);
            MyMethod(1, 2, d:4);

            MyMethod(1, 2, 3, 4);
            MyMethod(c: 3,       a: 1);
            MyMethod(c: 3,     d: 4,      a:1,     b:2);
            
        }
    }
}
