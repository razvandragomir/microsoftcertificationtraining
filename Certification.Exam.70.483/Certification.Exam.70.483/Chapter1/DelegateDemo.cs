﻿using System;
using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter1
{
    public class DelegateDemo:BaseDemo
    {
        class SimpleClass
        {
            public delegate void NameChangedDelegate(string oldName, string newName);
            public NameChangedDelegate NameChanged;


            private string _name;
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    if (_name == value)
                    {
                        return;
                    }

                    string oldName = _name;
                    _name = value;

                    if (NameChanged != null)
                    {
                        NameChanged(oldName, value);
                    }
                }
            }

        }

        public override void Run()
        {
            SimpleClass c = new SimpleClass();
            c.NameChanged += (oldName, newName) =>
            {
                Logger.Debug("Name changed from '{0}' to '{1}'", oldName, newName);
            };

            c.Name = "razvan";
            c.Name = "Marius";
           
        }

        void c_NameChanged(string oldName, string newName)
        {
            throw new NotImplementedException();
        }
    }
}
