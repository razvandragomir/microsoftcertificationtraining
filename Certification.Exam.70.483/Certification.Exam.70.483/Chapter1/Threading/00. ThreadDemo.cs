﻿using Certification.Exam.CSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Certification.Exam.CSharp.Chapter1.Threading
{
    public class ThreadDemo:BaseDemo
    {
        public override void Run()
        {
            Thread t = new Thread(DoSomeWork);
            t.Start();

            Logger.Debug("Waiting for Worker to finish");
            t.Join();
            Logger.Debug("Worker finished");
        }

        private void DoSomeWork()
        {
            Logger.Debug("DoSomeWork started");
            Thread.Sleep(1000);
            Logger.Debug("DoSomeWork finised");
        }

        // Question: in this example the DoSomeWork returns void. But what if we want to return a value instead?
    }
}
