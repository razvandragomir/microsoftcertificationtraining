﻿using Certification.Exam.CSharp.Core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Certification.Exam.CSharp.Chapter1.Threading
{
    public class DownloadAsyncDemo:BaseDemo
    {

        public override void Run()
        {

        }

        #region ReadFile example
        private byte[] buffer;
        private void StartReadFile()
        {
            var stream = File.OpenRead("");

            buffer = new byte[10];
            stream.BeginRead(buffer, 0, 10, ReadCallback, null);
        }

        private void ReadCallback(IAsyncResult ar)
        {
            // do something with buffer
        }
        #endregion

        #region Download web page

        private void StartDownloadGooglePage()
        {
            WebClient client = new WebClient();
            
            // the syncronous type:
            var content = client.DownloadString("http://www.google.com");

            // the event handler type
            client.DownloadStringCompleted += client_DownloadStringCompleted;
            client.DownloadStringAsync(new Uri("http://www.google.com"));

            
        }

        void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            // do something with e.Result
        }

        #endregion

        // question: isn't it ugly? No? .. then what about exception handling?
    }
}
