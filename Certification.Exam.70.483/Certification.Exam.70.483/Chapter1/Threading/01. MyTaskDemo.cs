﻿using Certification.Exam.CSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Certification.Exam.CSharp.Chapter1.Threading
{
    public class MyTaskDemo:BaseDemo
    {
        public override void Run()
        {
            MyTask<int> task = new MyTask<int>(DoSomeWork);
            task.Start();

            Logger.Debug("Waiting for Worker to finish");
            task.Wait();
            Logger.Debug("Worker finished");

            int result = task.Result;
            Logger.Debug("Result is {0}", result);
        }

        private int DoSomeWork()
        {
            Logger.Debug("DoSomeWork started");
            Thread.Sleep(1000);
            Logger.Debug("DoSomeWork finised");

            return 4;
        }
    
    }

    class MyTask<T> 
    {
        private Func<T> _action;
        private Thread _thread;

        private bool _executed = false;
        private T _result;
        
        public MyTask(Func<T> action)
        {
            _action = action;
        }

        public void Start()
        {
            if (_thread == null)
            {
                _thread = new Thread(ThreadWork);
                _thread.Start();
            }
        }

        public void Wait()
        {
            if (_thread != null)
            {
                _thread.Join();
            }
        }

        public T Result
        {
            get
            {
                if (_executed)
                {
                    return _result;
                }

                if (_thread == null)
                {
                    this.Start();
                }

                this.Wait();
                return _result;
            }
        }

        public void Cancel()
        {
            // yeeah, why not?
        }

        private void ThreadWork(object obj)
        {
            _result = _action();
            _executed = true;
        }

       
    }
}
