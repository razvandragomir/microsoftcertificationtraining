﻿using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter1
{
    public class TasksDemo:BaseDemo
    {
        public override void Run()
        {
            Stopwatch ceas = Stopwatch.StartNew();

            //Part1();
            Part2();

            ceas.Stop();
            Logger.Debug("Over: {0} ms", ceas.ElapsedMilliseconds);
        }

        private void Part1()
        {
            var task1 = WorkOne();
            var task2 = WorkTwo();
            var task3 = WorkThree(1000);

            int sum = task1 + task2 + task3;
            Logger.Debug("Sum: {0}", sum);
        }

        private void Part2()
        {
            var task1 = WorkOneAsync();
            var task2 = WorkTwoAsync();
            var task3 = WorkThreeAsync(1000);

            int sum = task1.Result + task2.Result + task3.Result;
            Logger.Debug("Sum: {0}", sum);
        }

        private int WorkOne()
        {
            Logger.Debug("WorkOne started");

            WebClient client = new WebClient();
            string content = client.DownloadString("http://google.com");

            Logger.Debug("WorkOne ended: {0}", content.Length);
            return content.Length;
        }

        private async Task<int> WorkOneAsync()
        {
            Logger.Debug("WorkOne started");
            
            WebClient client = new WebClient();
            string content = await client.DownloadStringTaskAsync("http://google.com");

            Logger.Debug("WorkOne ended: {0}", content.Length);
            return content.Length;
        }

        private int WorkTwo()
        {
            Logger.Debug("WorkTwo started");

            WebClient client = new WebClient();
            string content = client.DownloadString("http://microsoft.com");

            Logger.Debug("WorkTwo ended: {0}", content.Length);
            return content.Length;
        }
        private async Task<int> WorkTwoAsync()
        {
            Logger.Debug("WorkTwo started");

            WebClient client = new WebClient();
            string content = await client.DownloadStringTaskAsync("http://microsoft.com");

            Logger.Debug("WorkTwo ended: {0}", content.Length);
            return content.Length;
        }

        private int WorkThree(int a)
        {
            Logger.Debug("WorkThree started");

            Thread.Sleep(a);

            Logger.Debug("WorkThree ended: {0}", a);
            return a;
        }
        private async Task<int> WorkThreeAsync(int a)
        {
            Logger.Debug("WorkThree started");

            await Task.Delay(a);

            Logger.Debug("WorkThree ended: {0}", a);
            return a;
        }
    }
}
