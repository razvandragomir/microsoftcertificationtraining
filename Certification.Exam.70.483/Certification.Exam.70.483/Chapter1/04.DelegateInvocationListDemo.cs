﻿using System;
using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter1
{
    public class DelegateInvocationListDemo: BaseDemo
    {
        delegate void CalculationHandler(int a, int b);
        CalculationHandler Calculations;

        public override void Run()
        {
            Calculations += (a, b) => { };
            Calculations += (a, b) => { throw new Exception("OK"); };
            Calculations += calculation_sum;

            foreach (var c in Calculations.GetInvocationList())
            {
                var target = c.Target;
                var methodInfo = c.Method;
                try
                {
                    c.DynamicInvoke(3, 4);
                }
                catch (Exception ex)
                {

                }
            }
        }

        private void calculation_sum(int a, int b)
        {
            
        }
    }
}
