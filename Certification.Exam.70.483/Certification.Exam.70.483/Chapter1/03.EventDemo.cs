﻿using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter1
{
    public class EventDemo:BaseDemo
    {
        class SimpleClass
        {
            public delegate void CalculationDelegate(int a, int b);

            private CalculationDelegate _Calculation;

            public event CalculationDelegate Calculation
            {
                add
                {
                    Logger.Debug("Subscriber added");
                    _Calculation += value;
                }
                remove
                {
                    Logger.Debug("Subscriber removed");
                    _Calculation -= value;
                }
            }

            internal void MakeCalculations(int a, int b)
            {
                this._Calculation(a,b);
            }
        }


        public override void Run()
        {
            SimpleClass s = new SimpleClass();
            s.Calculation += (a, b) =>
            {
                Logger.Debug("Sum calculation: {0}", a+b);
            };
            s.Calculation += (a, b) =>
            {
                Logger.Debug("Substract calculation: {0},", a-b);
            };

            s.MakeCalculations(4,5);
        }
    }
}
