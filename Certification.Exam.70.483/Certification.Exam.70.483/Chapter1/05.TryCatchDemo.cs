﻿using System;
using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter1
{
    public class TryCatchDemo: BaseDemo
    {
        public override void Run()
        {
            string s = "just text34";
            int value = 0;

            try
            {
                value = int.Parse(s);
            }
            catch (ArgumentException)
            {
                Logger.Debug("Argument invalid");
            }
            catch (FormatException ex)
            {
                Logger.Debug(ex.Message);
                return;
            }
            catch (Exception ex)
            {
                value = 3;
            }
            finally
            {
                Logger.Debug("Finally executed");
            }

        }
    }
}
