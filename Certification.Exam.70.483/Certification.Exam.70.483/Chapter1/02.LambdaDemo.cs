﻿using System;
using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter1
{
    class LambdaDemo:BaseDemo
    {
        class Button
        {
            public Action OnClick;

            public void Click()
            {
                if (OnClick != null)
                {
                    OnClick();
                }
            }
        }

        public override void Run()
        {
            var buttons = new Button[10];
            for (int i = 0; i < 10; i++)
            {
                int id = i * 10;

                buttons[i] = new Button();
                
                // buttons[i].OnClick = button_OnClick;
                buttons[i].OnClick = () =>
                {
                    Logger.Debug("Button with id='{0}' was clicked", id);
                };
            }

            buttons[2].Click();
            buttons[4].Click();
        }

        void button_OnClick()
        {
            Logger.Debug("Where should I get the ID from inside this method?");
        }
    }
}
