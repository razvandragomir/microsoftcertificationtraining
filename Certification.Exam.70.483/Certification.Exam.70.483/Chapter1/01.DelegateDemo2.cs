﻿using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter1
{
    public class DelegateDemo2:BaseDemo
    {
        class SimpleClass
        {
            public delegate void NameChangedDelegate(string oldName, string newName);

            public event NameChangedDelegate NameChanged;

            private string _name;
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    if (_name == value)
                    {
                        return;
                    }

                    string oldName = _name;
                    _name = value;

                    if (NameChanged != null)
                    {
                        NameChanged(oldName, value);
                    }
                }
            }
        }

        public override void Run()
        {
            SimpleClass c = new SimpleClass();
            
            c.NameChanged += (oldName, newName) =>
            {
                Logger.Debug("Name changed from '{0}' to '{1}'", oldName, newName);
            };

            c.NameChanged += c_NameChanged;

            c.Name = "Razvan";
            c.Name = "Marius";

            // PART 2

            //c.NameChanged("hack", "you're dead!");
            Logger.Debug("The name is '{0}'", c.Name);

            c.NameChanged += (o, n) =>
            {
                Logger.Debug("What did just happend? This is the wrong message!");
            };
            c.Name = "New name";
        }

        void c_NameChanged(string oldName, string newName)
        {
            Logger.Debug("Another notification");
        }
    }
}
