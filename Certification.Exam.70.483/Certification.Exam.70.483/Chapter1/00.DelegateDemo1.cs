﻿using System;
using Certification.Exam.CSharp.Core;

namespace Certification.Exam.CSharp.Chapter1
{
    public class DelegateDemo1:BaseDemo
    {
        public delegate int CalculationDelegate(int a, int b);
        public delegate void WorkDelegate(long a); 

        public override void Run()
        {
            Part1();
            //Part2();
        }

        private void Part1()
        {
            CalculationDelegate calculationFunc = (a, b) =>
            {
                return a + b;
            };
            Logger.Debug("{0} + {1} = {2}", 3, 4, calculationFunc(3, 4));


            calculationFunc = (a, b) =>
            {
                return a * b;
            };
            Logger.Debug("{0} * {1} = {2}", 3, 4, calculationFunc(3, 4));


            calculationFunc = ACalculationMethod;

            Func<int, int, int> otherFunc = (a, b) =>
            {
                return a + b;
            };
            int c = otherFunc(3, 4);
        }

        private int ACalculationMethod(int a, int b)
        {
            return a / b;
        }

        private void Part2()
        {
            
        }
    }
}
