﻿using System;
using Certification.Exam.CSharp.Chapter1;
using Certification.Exam.CSharp.Core;
using Certification.Exam.CSharp.Chapter1.Threading;

namespace Certification.Exam.CSharp
{
    class Program
    {
        static void Main(string[] args)
        {
            BaseDemo demo = new MyTaskDemo();
            demo.Run();

            Console.ReadKey();
        }
    }
}
