﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfThreadingDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void DoSomething_Click(object sender, RoutedEventArgs e)
        {
            statusBlock.Text = "generating number with sync";
            int value = Go();
            statusBlock.Text = value.ToString();
        }


        private async void DoSomethingAsync_Click(object sender, RoutedEventArgs e)
        {
            statusBlock.Text = "generating number with async";
            int value = await GoAsync();
            statusBlock.Text = value.ToString();
        }

        private int Go()
        {
            Thread.Sleep(5000);

            WebClient client = new WebClient();
            string content = client.DownloadString("http://www.google.ro");

            return content.Length;
        }

        private async Task<int> GoAsync()
        {
            await Task.Delay(5000);

            WebClient client = new WebClient();
            string content = await client.DownloadStringTaskAsync("http://www.google.ro");

            return content.Length;
        }
    }
}
